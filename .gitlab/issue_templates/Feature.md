
### Quick explanation

> Simple description of the feature you need

### Priority/Importance

> What is the importance of the feature?
* critical (you would not use the system without)
* major (it would really help)
* minor (it would be nice to have it)

/label ~Feature
